<?php

use App\Livewire\Auth\Register;
use function Pest\Livewire\livewire;

it('should render', function () {
    livewire(Register::class)
        ->assertViewIs('auth.register');
});